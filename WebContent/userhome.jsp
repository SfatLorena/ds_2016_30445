<%@page import="java.util.List"%>
<%@page import="ds.assonetwo.service.FlightService"%>
<%@page import="java.util.Date"%>
<%@page import="ds.assonetwo.model.User"%>
<%@page import="ds.assonetwo.model.Flight"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<title>Result Page</title>
</head>
<body>
	<div>
		<div id="container">
			<div class="welcome">
				<h1>Flights</h1>
				<b>All flights</b><br />
				<%=new Date()%></br>
				<%
					User user = (User) session.getAttribute("user");
				%>
				<b>Welcome <%=user.getFirstName() + " " + user.getLastName()%></b> <br />
				<a href="logout.jsp">Logout</a>
				</p>
			</div>

			<h1>All flights currently in the database:</h1>
			<table>
				<thead>
					<tr>
						<th>Flight Number</th>
						<th>Departure</th>
						<th>Departure Time</th>
						<th>Arrival</th>
						<th>Arrival Time</th>
					</tr>
				</thead>
				<tbody>
					<%
						FlightService flightService = new FlightService();
						List<Flight> list = flightService.getAllFlights();
						for (Flight flight : list) {
					%>
					<tr>
						<td><%=flight.getFlightNumber()%></td>
						<td><%=flight.getDepartureCity().getName()%></td>
						<td><%=flight.getDepartureDateTime()%></td>
						<td><%=flight.getArrivalCity().getName()%></td>
						<td><%=flight.getArrivalDateTime()%></td>
					</tr>
					<%
						}
					%>
				
				<tbody>
			</table>
			<form method="post" action="ExternalResources">
			<label for="city1">Departure city:</label>
				<input id="city1" name="city1" class="form-city" title="city"
					value="" size="30" maxlength="50" /> 
					
			<label for="city2">Arrival city:</label>
					<input id="city2"
					name="city2" class="form-city" title="city" value="" size="30"
					maxlength="50" />
					<input type="submit" name="act2" value="Check Cities"
						id="checkCity" />
			</form>
			
		</div>
	</div>
</body>
</html>