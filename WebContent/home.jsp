<%@page import="java.util.List"%>
<%@page import="ds.assonetwo.service.FlightService"%>
<%@page import="java.util.Date"%>
<%@page import="ds.assonetwo.model.User"%>
<%@page import="ds.assonetwo.model.Flight"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<title>Result Page</title>
</head>
<body>
	<div>
		<div id="container">
			<div class="welcome">
				<h1>Flights</h1>
				<b>All flights</b><br />
				<%=new Date()%></br>
				<%
					User user = (User) session.getAttribute("user");
				%>
				<b>Welcome <%=user.getFirstName() + " " + user.getLastName()%></b> <br />
				<a href="logout.jsp">Logout</a>
				</p>
			</div>
			<p>Add a new flight</p>
			<form method="post" action="Flight">
				<div>
					<div>
						<label for="flightNumber">Flight Number</label> <input
							id="flightNumber" name="flightNumber" class="form-flight"
							title="flightNumber" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="departureCity">Departure City</label> <input
							id="departureCity" name="departureCity" class="form-flight"
							title="departureCity" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="departureDateTime">Departure Date and Time</label> <input
							id="departureDateTime" name="departureDateTime"
							class="form-flight" title="departureDateTime" value="" size="30"
							maxlength="50" />
					</div>
					<div>
						<label for="arrivalCity">Arrival City</label> <input
							id="arrivalCity" name="arrivalCity" class="form-flight"
							title="arrivalCity" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="arrivalDateTime">Arrival Date and Time</label> <input
							id="arrivalDateTime" name="arrivalDateTime" class="form-flight"
							title="arrivalDateTime" value="" size="30" maxlength="50" />
					</div>
					<input type="submit" name="act" value="AddFlight" id="addFlight" />
				</div>
			</form>
			<h1>All flights currently in the database:</h1>
			<table>
				<thead>
					<tr>
						<th>Flight Number</th>
						<th>Departure</th>
						<th>Departure Time</th>
						<th>Arrival</th>
						<th>Arrival Time</th>
					</tr>
				</thead>
				<tbody>
					<%
						FlightService flightService = new FlightService();
						List<Flight> list = flightService.getAllFlights();
						for (Flight flight : list) {
					%>
					<tr>
						<td><%=flight.getFlightNumber()%></td>
						<td><%=flight.getDepartureCity().getName()%></td>
						<td><%=flight.getDepartureDateTime()%></td>
						<td><%=flight.getArrivalCity().getName()%></td>
						<td><%=flight.getArrivalDateTime()%></td>
					</tr>
					<%
						}
					%>
				
				<tbody>
			</table>
			<br />

			<form method="post" action="Flight">
				<div>
					<div>
						<label for="flightNumber">Flight Number</label> <input
							id="flightNumber" name="flightNumber" class="form-flight"
							title="flightNumber" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="departureCity">Departure City</label> <input
							id="departureCity" name="departureCity" class="form-flight"
							title="departureCity" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="departureDateTime">Departure Date and Time</label> <input
							id="departureDateTime" name="departureDateTime"
							class="form-flight" title="departureDateTime" value="" size="30"
							maxlength="50" />
					</div>
					<div>
						<label for="arrivalCity">Arrival City</label> <input
							id="arrivalCity" name="arrivalCity" class="form-flight"
							title="arrivalCity" value="" size="30" maxlength="50" />
					</div>
					<div>
						<label for="arrivalDateTime">Arrival Date and Time</label> <input
							id="arrivalDateTime" name="arrivalDateTime" class="form-flight"
							title="arrivalDateTime" value="" size="30" maxlength="50" />
					</div>
					<input type="submit" name="act2" value="UpdateFlight"
						id="updateFlight" />
				</div>
			</form>

			<form method="post" action="Flight">
				<label for="flightToDelId">Flight Number of flight you want
					to delete</label> 
				<input id="flightToDelId" name="flightToDel"
					class="form-delete" title="flightToDel" value="" size="30"
					maxlength="50" /> 
				<input type="submit" name="act3"
					value="DeleteFlight" id="deleteFlight" />
			</form>
		</div>
	</div>
</body>
</html>