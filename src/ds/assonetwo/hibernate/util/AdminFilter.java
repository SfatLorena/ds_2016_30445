package ds.assonetwo.hibernate.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ds.assonetwo.model.User;

@WebFilter(urlPatterns = "/home.jsp")
public class AdminFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession(false);

		User us = (User) session.getAttribute("user");
		if (us != null) {
			if (us.getUserRole().name().equals("ADMIN")) {
				chain.doFilter(request, response);
			} else
				res.sendRedirect("login.jsp");
		} else
			res.sendRedirect("login.jsp");

	}

}
