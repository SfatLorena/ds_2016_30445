package ds.assonetwo.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.assonetwo.hibernate.util.HibernateUtil;
import ds.assonetwo.model.User;
import ds.assonetwo.model.UserRole;

public class UserDao {
    public void saveUser(String firstName, String lastName, String email, String userId, String password,UserRole ur) {
        Session session = HibernateUtil.openSession();
        Transaction transaction = null;
 
        try {
            transaction = session.beginTransaction();
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setUserId(userId);
            user.setPassword(password);
            user.setUserRole(ur);
            session.save(user);
            transaction.commit();
            System.out.println("Records inserted sucessessfully");
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
 
    }
 
    @SuppressWarnings("unchecked")
	public List<User> getAllUsers() 
    { 
        Session session = HibernateUtil.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<User> users = session.createQuery("from User").getResultList();
 
            for (Iterator iterator = users.iterator(); iterator.hasNext();) {
                User user = (User) iterator.next();
                System.out.println(user.getFirstName()+" "+user.getLastName());
            }
            transaction.commit();
            return users;
 
        } catch (HibernateException e) { 
            transaction.rollback(); 
            e.printStackTrace(); 
        } finally { 
            session.close(); 
        }
        return null;
    }
    
    public User getUser(String userid, String password){
    	Session session = HibernateUtil.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = (User)session.createQuery("from User where userId = '"+userid+"' and password = '"+password+"'").getResultList().get(0);
            transaction.commit();
            return user;
 
        } catch (HibernateException e) { 
            transaction.rollback(); 
            e.printStackTrace(); 
        } finally { 
            session.close(); 
        }
        return null;
    }
 
    public void deleteUser(Long id) {
 
        Session session = HibernateUtil.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String queryString = "from User where id = :id";
            User user = (User)session.createQuery("from User where id = :id")
            		.setParameter(0, id).getResultList().get(0);
            session.delete(user);
            System.out.println("User records deleted!");
 
        } catch (HibernateException e) { 
            transaction.rollback(); 
            e.printStackTrace(); 
        } finally { 
            session.close(); 
        }
    }
 
    public void updateUser(String firstName, String lastName, String email, String userId, String password) {
 
        Session session = HibernateUtil.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = (User)session.createQuery("from User where userId = :userId and password = :password")
            		.setParameter(0, userId).setParameter(1, password).getResultList().get(0);
            
            if(user != null){
            	if(!email.isEmpty())
            		user.setEmail(email);
            	if(!firstName.isEmpty())
            		user.setFirstName(firstName);
            	if(!lastName.isEmpty())
            		user.setLastName(lastName);
            }
            
            session.update(user);
            System.out.println("User records updated!");
        } catch (HibernateException e) { 
            transaction.rollback(); 
            e.printStackTrace(); 
        } finally { 
            session.close(); 
        }
    }

	@SuppressWarnings("finally")
	public User getUser(String userId) {
		Session session = HibernateUtil.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String queryString = "from User where id = :id";
            User user = (User)session.createQuery("from User where id = '"+userId+"'").getResultList().get(0);
            return user; 
        } catch (HibernateException e) { 
            transaction.rollback(); 
            e.printStackTrace(); 
        } finally { 
            session.close();
            return null;
        }
	}
}
