package ds.assonetwo.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.assonetwo.hibernate.util.HibernateUtil;
import ds.assonetwo.model.City;

public class CityDao {

	public void saveCity(City city){
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(city);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void saveCity(Long id, Long latitude, Long longitude, String name) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();

			City city = new City();
			city.setLatitude(latitude);
			city.setLongitude(longitude);
			city.setName(name);

			session.save(city);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public City getCity(String name) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();

			City city = (City) session.createQuery("from City where name = '" + name + "'").getResultList().get(0);

			transaction.commit();
			return city;

		} catch (

		HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public void deleteCity(String name) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();

			City city = (City) session.createQuery("from City where name = '" + name + "'").getResultList().get(0);
			session.delete(city);

			transaction.commit();

		} catch (

		HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void updateCity(String name, Long latitude, Long longitude) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();

			City city = (City) session.createQuery("from City where name = '" + name + "'").getResultList().get(0);

			session.update(city);

			transaction.commit();

		} catch (

		HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
