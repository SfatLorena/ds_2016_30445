package ds.assonetwo.dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import ds.assonetwo.hibernate.util.HibernateUtil;
import ds.assonetwo.model.Flight;

public class FlightDao {

	public void saveFlight(Flight flight) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(flight);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void saveFlight(int flightNumber, String airplaneType, Long departureCityId, Date departureDateTime,
			Long arrivalCityId, Date arrivalDateTime) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			Flight flight = new Flight();
			flight.setFlightNumber(flightNumber);
			flight.setAirplaneType(airplaneType);
			flight.setDepartureDateTime(departureDateTime);
			flight.setArrivalDateTime(arrivalDateTime);
			session.save(flight);
			transaction.commit();
			System.out.println("Records inserted sucessessfully");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Flight> getAllFlights() {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<Flight> flights = session.createQuery("from Flight").getResultList();

			/*
			 * for (Iterator iterator = flights.iterator(); iterator.hasNext();)
			 * { Flight flight = (Flight) iterator.next();
			 * //System.out.println(flight.getDepartureCity().getName()+" "
			 * +flight.getArrivalCity().getName()); }
			 */
			transaction.commit();
			return flights;

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public Flight getFlight(int flightNumber) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Flight flight = (Flight) session.createQuery("from Flight where flightNumber = '" + flightNumber + "'")
					.getResultList().get(0);
			transaction.commit();
			return flight;

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public void deleteFlight(int flightNumber) {

		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Flight flight = (Flight) session.createQuery("from Flight where flightNumber = '" + flightNumber + "'")
					.getResultList().get(0);
			session.delete(flight);
			transaction.commit();
			System.out.println("Flight records deleted!");

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void updateFlight(Flight flight) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			System.out.println("Flight to update + "+ flight.getFlightNumber());
			Flight flightDb = (Flight) session
					.createQuery("from Flight where flightNumber = '" + flight.getFlightNumber() + "'").getResultList()
					.get(0);

			//flight.setId(flightDb.getId());
			System.out.println("From DB + "+ flightDb.getId());
			/*Flight flightSession = session.get(Flight.class, flightDb.getId());*/
			
			flightDb.setAirplaneType(flight.getAirplaneType());
			flightDb.setArrivalCity(flight.getArrivalCity());
			flightDb.setArrivalDateTime(flight.getArrivalDateTime());
			flightDb.setDepartureCity(flight.getDepartureCity());
			flightDb.setDepartureDateTime(flight.getArrivalDateTime());
			flightDb.setFlightNumber(flight.getFlightNumber());

			session.update(flightDb);
			transaction.commit();

			System.out.println("Flight records updated!");
		} catch (HibernateException e) {
			
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void updateFlight(int flightNumber, String airplaneType, Long departureCityId, Date departureDateTime,
			Long arrivalCityId, Date arrivalDateTime) {

		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Flight flight = (Flight) session.createQuery("from Flight where flightId = '" + flightNumber + "'")
					.getResultList().get(0);

			if (!airplaneType.isEmpty()) {
				flight.setAirplaneType(airplaneType);
			}
			/*
			 * if(departureCityId != null){
			 * flight.setDepartureCityId(departureCityId); } if(arrivalCityId !=
			 * null){ flight.setArrivalCityId(arrivalCityId); }
			 */
			if (departureDateTime != null) {
				flight.setDepartureDateTime(departureDateTime);
			}
			if (arrivalDateTime != null) {
				flight.setArrivalDateTime(arrivalDateTime);
			}

			session.update(flight);
			System.out.println("Flight records updated!");
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
