package ds.assonetwo.service;

import java.util.List;

import ds.assonetwo.dao.FlightDao;
import ds.assonetwo.model.Flight;

public class FlightService {
	private FlightDao flightDao;
	
	public FlightService(){
		flightDao = new FlightDao();
	}
	
	public void addNewFlight(Flight flight){
		flightDao.saveFlight(flight);
	}
	
	public Flight getFlight(int flightNumber){
		return flightDao.getFlight(flightNumber);
	}
	
	public List<Flight> getAllFlights(){
		return flightDao.getAllFlights();
	}
	
	public void deleteFlight(int id){
		flightDao.deleteFlight(id);
	}
	
	public void updateFlight(Flight flight){
		flightDao.updateFlight(flight);
	}
}
