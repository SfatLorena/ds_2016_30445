package ds.assonetwo.service;

import ds.assonetwo.dao.CityDao;
import ds.assonetwo.model.City;

public class CityService {
	private CityDao cityDao;
	
	public CityService(){
		cityDao = new CityDao();
	}
	
	public void addNewCity(City city){
		cityDao.saveCity(city);
	}
	
	public City getCity(String name){
		return cityDao.getCity(name);
	}
	
	public void deleteCity(String name){
		cityDao.deleteCity(name);
	}
	
	public void updateCity(City city){
		cityDao.updateCity(city.getName(), city.getLatitude(), city.getLongitude());
	}
}
