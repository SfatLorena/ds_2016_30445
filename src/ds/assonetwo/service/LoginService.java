package ds.assonetwo.service;

import java.util.List;

import ds.assonetwo.dao.UserDao;
import ds.assonetwo.model.User;
 
public class LoginService {
	
	UserDao userDao;	
	User currentUser;
 
    public LoginService() {
		userDao = new UserDao();
	}

	public boolean authenticateUser(String userId, String password) {
        currentUser = userDao.getUser(userId, password);         
        if(currentUser!=null){
            return true;
        }else{
            return false;
        }
    }
	
	public User getCurrentUser(String userId){
		return currentUser;
	}
	public List<User> getListOfUsers(){
		return userDao.getAllUsers();
	}
	
	
}