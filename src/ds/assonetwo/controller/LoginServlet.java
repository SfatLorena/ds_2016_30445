package ds.assonetwo.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ds.assonetwo.model.User;
import ds.assonetwo.service.LoginService;

public class LoginServlet extends HttpServlet {

	LoginService loginService = new LoginService();

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		boolean result = loginService.authenticateUser(userId, password);
		User user = loginService.getCurrentUser(userId);
		
		if (result == true) {

			if (user.getUserRole().name().equals("ADMIN")) {
				request.getSession().setAttribute("user", user);
				response.sendRedirect("home.jsp");
			}
			else if(user.getUserRole().name().equals("USER")) {
				request.getSession().setAttribute("user", user);
				response.sendRedirect("userhome.jsp");
			}
		} else {
			response.sendRedirect("error.jsp");
		}

	}

	public void doGet(HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

}