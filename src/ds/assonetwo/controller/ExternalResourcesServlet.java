package ds.assonetwo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import ds.assonetwo.dao.CityDao;
import ds.assonetwo.model.City;

public class ExternalResourcesServlet extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String departureCity = request.getParameter("city1");
		String arrivaleCity = request.getParameter("city2");
		CityDao cdao = new CityDao();
		City cityDep = cdao.getCity(departureCity);
		// City cityArr = cdao.getCity(arrivaleCity);

		String url = "http://new.earthtools.org/timezone" + "/" + cityDep.getLatitude() + "/" + cityDep.getLongitude();

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet req = new HttpGet(url);

		// add request header
		HttpResponse res = client.execute(req);

		BufferedReader rd = new BufferedReader(new InputStreamReader(res.getEntity().getContent()));
		String line;
		while ((line = rd.readLine()) != null) {
			if (line.contains("<localtime>")) {
				break;
			}
		}
		line = line.substring(line.lastIndexOf(' ') + 1);
		line = line.substring(0, line.indexOf('<'));
		System.out.println(line);
	}

}
