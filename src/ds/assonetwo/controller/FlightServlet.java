package ds.assonetwo.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ds.assonetwo.model.Flight;
import ds.assonetwo.model.User;
import ds.assonetwo.service.CityService;
import ds.assonetwo.service.FlightService;
import ds.assonetwo.service.LoginService;
import javafx.scene.input.DataFormat;

public class FlightServlet extends HttpServlet {

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	FlightService flightService = new FlightService();
	CityService cityService = new CityService();
	LoginService loginService = new LoginService();

	// User user = (User) session.getAttribute("user");
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * HttpSession session = request.getSession(); User user = (User)
		 * session.getAttribute("user"); boolean result =
		 * loginService.authenticateUser(user.getUserId(), user.getPassword());
		 * if(result == false){ response.sendRedirect("error.jsp"); }
		 */
		String button = request.getParameter("act");
		String button1 = request.getParameter("act2");
		if ((button != null) || (button1 != null)) {
			int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
			String airplaneType = request.getParameter("airplaneType");
			String departureCity = request.getParameter("departureCity");
			Date departureDateTime = new Date();
			try {
				departureDateTime = df.parse(request.getParameter("departureDateTime"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String arrivalCity = request.getParameter("arrivalCity");
			Date arrivalDateTime = new Date();
			try {
				arrivalDateTime = df.parse(request.getParameter("arrivalDateTime"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Flight flight = new Flight();
			flight.setAirplaneType(airplaneType);
			flight.setArrivalCity(cityService.getCity(arrivalCity));
			flight.setArrivalDateTime(arrivalDateTime);
			flight.setDepartureCity(cityService.getCity(departureCity));
			flight.setDepartureDateTime(departureDateTime);
			flight.setFlightNumber(flightNumber);
			flight.setAirplaneType("boeing");

			System.out.println(button);
			System.out.println(button1);

			if (button1 != null) {
				flightService.updateFlight(flight);
			} else if (button != null) {
				flightService.addNewFlight(flight);
			}
		}
		//else
		String button3 = request.getParameter("act3");
		if(button3 != null){
			System.out.println(request.getParameter("flightToDel"));
			int flightNumber = Integer.parseInt(request.getParameter("flightToDel"));
			flightService.deleteFlight(flightNumber);
		}
		response.sendRedirect("home.jsp");
	}

}
